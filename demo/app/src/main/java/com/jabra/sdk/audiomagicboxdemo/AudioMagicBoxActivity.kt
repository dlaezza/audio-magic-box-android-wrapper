package com.jabra.sdk.audiomagicboxdemo

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jabra.sdk.audiomagicbox.MySoundEq
import com.jabra.sdk.audiomagicbox.MySoundEqBand

import kotlinx.android.synthetic.main.activity_audio_magic_box.*
import kotlinx.android.synthetic.main.content_main.*

class AudioMagicBoxActivity : AppCompatActivity() {
    companion object {
        private val left = floatArrayOf(8.49f, 17.27f, 22.45f, 22.71f, 23.82f, 19.51f, 12.72f, 6.27f, 16.42f, 14.56f, 22.89f)
        private val right = floatArrayOf(11.07f, 20.21f, 20.02f, 23.93f, 19.38f, 12.03f, 7.20f, 5.52f, 6.86f, 13.93f, 22.39f)

        private fun doMagicBox() :List<MySoundEqBand> {
            val eq = MySoundEq(0X24A5, left, right)
            return eq.compute()
        }
    }

    class BandsAdapter(private val bands :List<MySoundEqBand>) :
        RecyclerView.Adapter<BandsAdapter.ViewHolder>() {

        class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
            private val freq = view.findViewById(R.id.freq) as TextView
            private val gain = view.findViewById(R.id.gain) as TextView

            fun setBand(band :MySoundEqBand) {
                freq.text = band.frequency.toString()
                gain.text = band.gain.toString()
            }
        }

        override fun getItemCount(): Int {
            return bands.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.eq_band, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.setBand(bands[position])
        }
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_audio_magic_box)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        left_audiogram.text = left.joinToString(prefix = "[", postfix = "]")
        right_audiogram.text = right.joinToString(prefix = "[", postfix = "]")

        bands.setHasFixedSize(false)
        bands.layoutManager = LinearLayoutManager(this)

        run.setOnClickListener { bands.adapter = BandsAdapter(doMagicBox()) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
