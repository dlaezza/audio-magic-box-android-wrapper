/*
    MySound Compute_EQ, blame szabetian@jabra.com
    ==============
    Computing MySound optimized personalized EQ gains.
    Input: dBHL values at {125, 250, 500, 750, 1000, 1500, 2000, 3000, 4000, 6000, 8000}
    Output: Optimized gains for a 10-band biquad to achieve desired command gains - removes 10th band (6kHz)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "compute_eq.h"
#include "calibrations.h"

// Tuning arrays
#define AG_BAND_COEFFS {0.20f, 0.28f, 0.40f, 0.30f, 0.30f, 0.26f}
#define DBHL_FREQS {125.0f, 250.0f, 500.0f, 750.0f, 1000.0f, 1500.0f, 2000.0f, 3000.0f, 4000.0f, 6000.0f, 8000.0f}

// Static function declerations
static void tune_dBHL(t_mysound_data *mysound_data);
static void tune_bands(t_mysound_data *mysound_data);
static void remove_band(t_mysound_data *mysound_data, float *personalised_output, float *opt_freqs, int size, int remove_pos);
static void optimized_gain_calc(t_mysound_data *mysound_data, float *cmd_gains, float *B);

// Static variable declerations
static float B[MYSOUND__B_MATRIX_SIZE] = B_MATRIX; // Interaction matrix
static float freq_conv[MYSOUND__AG_SIZE] = DBHL_FREQS; // Used for 11->10 frequency array conversion
static const int remove_pos = 10; // Element to be removed (Corresponds to 6kHz) -> Convert to 10-band output

// Calibrations
static float calib_emma[MYSOUND__TUNING_BANDS] = EMMA_TUNING;
static float calib_titan[MYSOUND__TUNING_BANDS] = TITAN_TUNING;

int mysound_init(t_mysound_data *mysound_data, float *calib_type, float *ag_left, float *ag_right) {
    // Reset mysound struct to zeros (for android)
    memset(mysound_data, 0, sizeof(t_mysound_data));

    // Checking preconditions
    mysound_data->init_ok = 0;

    // Use calibrations header
    for (int i = 0; i < MYSOUND__TUNING_BANDS; i++) {
        mysound_data->calib_array[i] = calib_type[i];
    }

    // Read L/R input data and store
    for (int i = 0; i < MYSOUND__AG_SIZE; i++) {
        mysound_data->ag_left[i] = ag_left[i];
        mysound_data->ag_right[i] = ag_right[i];
    }

    // dBHL tuning parameters
    mysound_data->tan_slope = 3.4;
    mysound_data->dBHL_scalar = 16;

    // Tuning initialisation, fill ag_band and ag_dbhl coeffs with tuning arrays
    float loc_band_coeffs[MYSOUND__TUNING_BANDS] = AG_BAND_COEFFS;
    for (int i = 0; i < MYSOUND__TUNING_BANDS; i++) {
        mysound_data->ag_band_coeffs[i] = loc_band_coeffs[i];
    }

    mysound_reset(mysound_data);
    mysound_data->init_ok = 1;
    return 0;
}

void mysound_reset(t_mysound_data *mysound_data) {
    // Reset output
    for (int i = 0; i < MYSOUND__AG_SIZE; i++) {
        mysound_data->personalised_output[i] = 0.0f;
    }
}

void mysound_apply(t_mysound_data *mysound_data) {
    // Element-wise mean of audiogram
    for (int i = 0; i < MYSOUND__AG_SIZE; i++) {
        mysound_data->ag_mean[i] = (mysound_data->ag_left[i] + mysound_data->ag_right[i]) / 2;
        mysound_data->ag_mean[i] = mysound_data->ag_mean[i] / 100.0; // Scale to within [0-100] for dBHL tuning

        // Make sure dBHL values are >= 0
        if (mysound_data->ag_mean[i] < 0) {
            mysound_data->ag_mean[i] = 0;
        }
    }

    // Apply tuning functions
    tune_dBHL(mysound_data);
    tune_bands(mysound_data);

    // Calculate tuned result curve -> If extra calibration is required, simply add calibration vector to ag_bands
    for (int i = 0; i < MYSOUND__AG_SIZE; i++) {
        mysound_data->personalised_output[i] = mysound_data->ag_bands[i]; // + mysound_data->calib_array[i];
    }

    // Remove 10th band (correponding to 6kHz) from output
    remove_band(mysound_data, mysound_data->personalised_output, mysound_data->opt_freqs, MYSOUND__AG_SIZE, remove_pos);

    // Generate optimized gains based on command gains from magic box output
    optimized_gain_calc(mysound_data, mysound_data->cmd_gains, B);

}

static void tune_dBHL(t_mysound_data *mysound_data) {
    // Gain sensitivity tuning based on a fine-tuned tanh
    for (int i = 0; i < 11; i++) {
        mysound_data->ag_dbhl[i] = tanh(mysound_data->ag_mean[i] * mysound_data->tan_slope);
        mysound_data->ag_dbhl[i] *= mysound_data->dBHL_scalar;
    }
}

static void tune_bands(t_mysound_data *mysound_data) {
    // Tuning by frequency bands
    for (int i = 0; i < MYSOUND__AG_SIZE; i++) {
        if (i <= 1) {
            mysound_data->ag_bands[i] = (mysound_data->ag_dbhl[i] * mysound_data->ag_band_coeffs[0]) * mysound_data->calib_array[0];
        }
        else if (i > 1 && i <= 3) {
            mysound_data->ag_bands[i] = (mysound_data->ag_dbhl[i] * mysound_data->ag_band_coeffs[1]) * mysound_data->calib_array[1];
        }
        else if (i > 3 && i <= 5) {
            mysound_data->ag_bands[i] = (mysound_data->ag_dbhl[i] * mysound_data->ag_band_coeffs[2]) * mysound_data->calib_array[2];
        }
        else if (i > 5 && i <= 7) {
            mysound_data->ag_bands[i] = (mysound_data->ag_dbhl[i] * mysound_data->ag_band_coeffs[3]) * mysound_data->calib_array[3];
        }
        else if (i > 7 && i <= 9) {
            mysound_data->ag_bands[i] = (mysound_data->ag_dbhl[i] * mysound_data->ag_band_coeffs[4]) * mysound_data->calib_array[4];
        }
        else if (i > 9 && i <= 11) {
            mysound_data->ag_bands[i] = (mysound_data->ag_dbhl[i] * mysound_data->ag_band_coeffs[5]) * mysound_data->calib_array[5];
        }
    }
}

static void remove_band(t_mysound_data *mysound_data, float *personalised_output, float *opt_freqs, int size, int remove_pos) {
    // Remove the Nth band (pos) from the personalised output and frequency output array
    if (remove_pos < 0 || remove_pos > size) {
        return;
    } else {
        remove_pos -= 1;
        for (int i = remove_pos; i < size-1; i++) {
            mysound_data->personalised_output[i] = personalised_output[i + 1];
            freq_conv[i] = freq_conv[i + 1];
        }
        size--;
    }

    // New 10 element array of command gains
    for (int i = 0; i < MYSOUND__OPT_GAINS_SIZE; i++) {
        mysound_data->cmd_gains[i] = personalised_output[i];
        mysound_data->opt_freqs[i] = freq_conv[i];
    }

}

static void optimized_gain_calc(t_mysound_data *mysound_data, float *cmd_gains, float *B) {
    // Initialize to zeros
    for (int i = 0; i < MYSOUND__OPT_GAINS_SIZE; i++) {
        mysound_data->opt_gains[i] = 0;
    }

    // Calculate optimized gains
    for (int i = 0; i< MYSOUND__OPT_GAINS_SIZE; i++) {
        for (int j = 0; j < MYSOUND__OPT_GAINS_SIZE; j++) {
            mysound_data->opt_gains[j] += B[i + (j*MYSOUND__OPT_GAINS_SIZE)] * cmd_gains[i];
        }
    }
}

/*  App-team demo ================

int main() {
    t_mysound_data data; // Instance of struct
    t_mysound_data *data_ptr = &data; // Pointer to struct address

    // Audiogram results L/R
    float left[11] = {8.49, 17.27, 22.45, 22.71, 23.82, 19.51, 12.72, 6.27, 16.42, 14.56, 22.89};
    float right[11] = {11.07, 20.21, 20.02, 23.93, 19.38, 12.03, 7.20, 5.52, 6.86, 13.93, 22.39};

    mysound_init(data_ptr, calib_emma, left, right);
    mysound_apply(data_ptr);

    // Output of magic-box -> optimized gains. These are the parameters to be sent to MySound EQ.
    for (int i = 0; i < MYSOUND__OPT_GAINS_SIZE; i++) {
        printf("Opt Band %d: %.4f\n", i+1, data_ptr->cmd_gains[i]);
    }

    // Output frequency spacings (10 bands): Audiogram[11] without the 10th element corresponding to 6kHz.
    for (int i = 0; i < MYSOUND__OPT_GAINS_SIZE; i++) {
        printf("Freq Band %i: %.0f\n", i+1, data_ptr->opt_freqs[i]);
    }

    return 0;
}

*/
