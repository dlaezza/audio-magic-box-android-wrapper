#!/usr/bin/env sh

# This script builds CMake projects for Android.
#
# Read environment variables:
#   - ANDROID_NDK - Path to the Android NDK
#
# Arguments:
#   --android-platform  Android API level to build for. Defaults to 21.
#   --android-stl       STL library to be used for the shared native library.
#                       Defaults to 'c++_static'. Check this link for the
#                       available values: https://developer.android.com/ndk/guides/cmake#android_stl
#   --build-prefix      CMake build directory prefix. Defaults to 'build'.
#   --build-type        CMake build type. Defaults to 'Debug'.
#   --cmake-generator   CMake generator. Defaults to 'Ninja'.
#   --install-prefix    CMake install directory prefix. Defaults to 'install'.
#   --jar               Path to the produced Jar file. If relative, it is
#                       prefixed with $INSTALL_PREFIX.
#   --java-path         Directory where the generated Java files are placed.
#                       If relative, it is prefixed with $BUILD_PREFIX.
#                       Defaults to $BUILD_PREFIX/java.
#   --source-path       CMake source directory. Defaults to ..
#                       (the top-level directory of this project).
#   --swig-sources      Directory where swig source interface files are
#                       located.
#   $@:                 Android ABIs the project should be built for. Defaults
#                       to: 'x86 x86_64 arm64-v8a armeabi-v7a'
#
# CMake alone is not sufficient, as we need to use the Android CMake toolchain
# file. We could do that with CMake ExternalProject, but this prevents us from
# using the targets defined in the rest of the CMake machinery.

set -e

#######################################
# Requirements
#######################################

CMAKE_MIN_VERSION='3.13'
CMAKE_INSTALLED_VERSION="$(cmake --version | head -n 1 | cut -d ' ' -f 3)"

[ "$(printf "$CMAKE_MIN_VERSION\n$CMAKE_INSTALLED_VERSION\n" \
    | sort -V | head -n 1)" != "$CMAKE_MIN_VERSION" ] && {

    printf 'This script requires CMake >= %s, but %s was found.' \
        "$CMAKE_MIN_VERSION" "$CMAKE_INSTALLED_VERSION"
    exit 1
}

#######################################
# Functions
#######################################

make_absolute() {
    PTH="$1"
    PREFIX="$2"

    [ "$(echo "$PTH" | cut -c 1)" != '/' ] && PTH="${PREFIX}/${PTH}"

    echo "$PTH"

    unset PTH PREFIX
}

#######################################
# Local variables
#######################################

# Current working directory
PWD="$(readlink -f .)"

# The Android CMake toolchain file to be used to build
ANDROID_TOOLCHAIN="${ANDROID_NDK:?}/build/cmake/android.toolchain.cmake"

#######################################
# Input processing
#######################################

# Setting default values
ABIS='x86 x86_64 arm64-v8a armeabi-v7a'
ANDROID_PLATFORM=21
ANDROID_STL=c++_static
BUILD_PREFIX="$PWD/build"
BUILD_TYPE='Debug'
CMAKE_GENERATOR='Ninja'
INSTALL_PREFIX="$PWD/install"
JAVA_PATH='java'
SOURCE_PATH="$PWD/.."

# Processing named parameters
while [ $# -gt 0 ]; do
    case "$1" in

        '--android-platform')
            shift
            ANDROID_PLATFORM="$1"
            ;;

        '--android-stl')
            shift
            ANDROID_STL="$1"
            ;;

        '--build-prefix')
            shift
            BUILD_PREFIX="$(make_absolute "$1" "$PWD")"
            ;;

        '--build-type')
            shift
            BUILD_TYPE="$1"
            ;;

        '--cmake-generator')
            shift
            CMAKE_GENERATOR="$1"
            ;;

        '--install-prefix')
            shift
            INSTALL_PREFIX="$(make_absolute "$1" "$PWD")"
            ;;

        '--jar')
            shift
            JAR_FILE="$1"
            ;;

        '--java-path')
            shift
            JAVA_PATH="$1"
            ;;

        '--source-path')
            shift
            SOURCE_PATH="$(make_absolute "$1" "$PWD")"
            ;;

        '--swig-sources')
            shift
            SWIG_SOURCES="$(make_absolute "$1" "$PWD")"
            ;;

        *)
            break
            ;;
    esac

    shift
done

# Checking named parameters after positional
echo "$@" | grep '\s\+--' > /dev/null 2>&1 && {
    echo >&2 'Named arguments after positional!'
    exit 1
}

# Processing positional parameters
[ $# -gt 0 ] && {
    ABIS="$@"
}

# Emptiness check for arguments without defaults
[ -z "$JAR_FILE" ] && {
    echo >&2 'No Jar file specified!'
    exit 1
}

# Further input processing
JAR_FILE="$(make_absolute "${JAR_FILE:?}" "${INSTALL_PREFIX:?}")"
JAVA_PATH="$(make_absolute "${JAVA_PATH:?}" "${INSTALL_PREFIX:?}")"

#######################################
# Android shared libraries
#######################################

for ABI in $ABIS; do
    # CMake build path
    BUILD_PATH="${BUILD_PREFIX:?}/Android-$ABI-$BUILD_TYPE"

    cmake \
        -DANDROID_ABI="${ABI:?}" \
        -DANDROID_STL="${ANDROID_STL:?}" \
        -DANDROID_PLATFORM="${ANDROID_PLATFORM:?}" \
        -DCMAKE_BUILD_TYPE="${BUILD_TYPE:?}" \
        -DCMAKE_INSTALL_PREFIX="${INSTALL_PREFIX:?}" \
        -DCMAKE_TOOLCHAIN_FILE="${ANDROID_TOOLCHAIN:?}" \
        -DJABRA_ANDROID=True \
        -DJABRA_NO_INTERNAL_CONAN=True \
        -DJAVA_INSTALL_PATH="${JAVA_PATH:?}" \
        -DSHARED_LIB_INSTALL_PATH="${INSTALL_PREFIX:?}/lib/${ABI:?}" \
        -DSWIG_OUTPUT_PATH="${BUILD_PREFIX:?}/swig" \
        -DSWIG_SOURCES_DIR="$SWIG_SOURCES" \
        -G "${CMAKE_GENERATOR:?}" \
        -S "${SOURCE_PATH:?}" \
        -B "${BUILD_PATH:?}"
    cmake --build "${BUILD_PATH:?}" --target install
done

#######################################
# Jar creation
#######################################

# Sources compilation
echo 'Compiling generated Java/Kotlin files'

JAVA_FILES="$(find "${JAVA_PATH:?}" -type f -name '*.java')"
KOTLIN_FILES="$(find "${JAVA_PATH:?}" -type f -name '*.kt')"

[ -z "$JAVA_FILES" ] && {
    echo >&2 'Error: no Java files found after swig generation'
    exit 1
}

javac ${JAVA_FILES:?}
[ -n "${KOTLIN_FILES:?}" ] \
    && kotlinc -d "${JAR_FILE:?}" ${JAVA_FILES:?} ${KOTLIN_FILES:?}

# Jar creation
echo 'Creating Jar file from the compiled sources'

# Kotlin compiles to a jar, so the file might already exist
if [ -e "${JAR_FILE:?}" ]; then
    # Updating Jar archive
    JAR_CMD='u'
else
    # Creating Jar archive
    JAR_CMD='c'
fi

cd "${JAVA_PATH:?}"
find . -type f -name '*.class' | xargs jar "${JAR_CMD:?}f" "${JAR_FILE:?}"
cd - > /dev/null 2>&1

echo 'Adding shared libraries to jar'
cd "${INSTALL_PREFIX:?}"
for ABI in $ABIS; do
    jar uf "${JAR_FILE:?}" lib/$ABI/lib*.so
done
cd - > /dev/null 2>&1

echo 'Jar created'
