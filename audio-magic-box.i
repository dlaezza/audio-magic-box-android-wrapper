%module AudioMagicBox

// <<<<<<<<<< Swig libraries >>>>>>>>>>
%include <arrays_java.i>
%include <cpointer.i>
%include <enums.swg>
%include <java.swg>

// <<<<<<<<<< Global Java setup >>>>>>>>>>
%javaconst(1);

// <<<<<<<<<< Renames >>>>>>>>>>
%rename("%(lowercamelcase)s", %$isfunction) "";
%rename("%(lowercamelcase)s", %$ismember) "";
%rename("%(lowercamelcase)s", %$isglobal) "";

// <<<<<<<<<< Ignores >>>>>>>>>>
/*
    This is C, everything is public. Therfore, we make everything private by
    default by ignoring all members and whitelisting public ones
*/
%rename($ignore, %$ismember) "";

// <<<<<<<<<< Typemaps >>>>>>>>>>
%apply float[] { float * };

// <<<<<<<<<< Generated Java setup >>>>>>>>>>

%typemap(javaclassmodifiers) SWIGTYPE "class";

%pragma(java) jniclassclassmodifiers="class"
%pragma(java) jniclasscode=%{
    static {
        try {
            System.loadLibrary(LibraryHelper.name);
        } catch (UnsatisfiedLinkError e) {
            System.err.println(\"Native code library failed to load. \\n\"
                + e);
            System.exit(1);
        }
    }
%}

%pragma(java) moduleclassmodifiers="class"

// <<<<<<<<<< Includes for C wrapper file >>>>>>>>>>
%{
#include "magic-box/compute_eq.h"
%}

// <<<<<<<<<< Constants >>>>>>>>>>
%rename($ignore, regexmatch$name="^MYSOUND__") "";
%rename("OUTPUT_SIZE") "MYSOUND__OPT_GAINS_SIZE";

%include "magic-box/calibrations.h"
%constant const float calib_emma[MYSOUND__TUNING_BANDS] = EMMA_TUNING;
%constant const float calib_titan[MYSOUND__TUNING_BANDS] = TITAN_TUNING;

// <<<<<<<<<< Enums >>>>>>>>>>
// The enum needs not to be exposed
%ignore enum_mysound_calibration_type;

// <<<<<<<<<< Functions >>>>>>>>>>
%ignore mysound_reset;

// <<<<<<<<<< Structs >>>>>>>>>>
/*
    We can't use generic rename here, was we would need both camelcase and
    stripping of t_.
*/
%rename("MySoundData") t_mysound_data;

// t_mysound_data public members

%immutable opt_freqs;
%rename("%(lowercamelcase)s") opt_freqs;

%immutable opt_gains;
%rename("%(lowercamelcase)s") opt_gains;

%include "magic-box/compute_eq.h"
