package com.jabra.sdk.audiomagicbox;

data class MySoundEqBand(val frequency :Float, val gain :Float)
