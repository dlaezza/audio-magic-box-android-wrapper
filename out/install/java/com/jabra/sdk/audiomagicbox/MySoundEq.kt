package com.jabra.sdk.audiomagicbox

open class MySoundEq(pid :Int, leftAudiogram :FloatArray, rightAudiogram :FloatArray) {
    companion object {
        protected val pidEmma = 0x24a5
        protected val pidTitan = 0x24a7
    }

    private val mySoundData = MySoundData()
    protected val calib = when (pid) {
        pidEmma -> AudioMagicBox.calibEmma
        pidTitan -> AudioMagicBox.calibTitan
        else -> AudioMagicBox.calibTitan
    }

    init {
        AudioMagicBox.mysoundInit(mySoundData, calib, leftAudiogram, rightAudiogram)
    }

    open fun compute() :List<MySoundEqBand> {
        AudioMagicBox.mysoundApply(mySoundData)
        return mySoundData.optFreqs.zip(mySoundData.optGains, ::MySoundEqBand)
    }
}
